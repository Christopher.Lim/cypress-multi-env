export class Environments {

    getEnv() {
        const env = Cypress.env('ENV');

        if (env == 'dev') {
            return 'https://www.cypress.io/features';
        } else if (env == 'qa') {
            return 'https://www.cypress.io/how-it-works';
        } else if (env == 'qa-prod') {
            return 'https://www.cypress.io/dashboard';
        } else {
            return 'https://www.cypress.io/';
        }

    }
}
